#!/bin/bash

build_project=${CODE_BUILD_PROJECT}
build_id=$(aws codebuild start-build --project-name $build_project --query 'build.id' --output text)
build_status=$(aws codebuild batch-get-builds --ids $build_id --query 'builds[].buildStatus' --output text)

while [ $build_status == "IN_PROGRESS" ] 
do
    sleep 10
    build_status=$(aws codebuild batch-get-builds --ids $build_id --query 'builds[].buildStatus' --output text)
done

stream_name=$(aws codebuild batch-get-builds --ids $build_id --query 'builds[].logs.streamName' --output text)
group_name=$(aws codebuild batch-get-builds --ids $build_id --query 'builds[].logs.groupName' --output text)

aws logs get-log-events --log-stream-name $stream_name --log-group-name $group_name --query 'events[].message' --output text
echo Codebuild completed with status $build_status
